import { connect } from 'react-redux';

import Task from '../components/Task';
import ToDoList from '../components/ToDoList';

import { deleteItem } from '../redux/todo';
import { checkItem } from '../redux/todo';
import { tickImportantTask } from '../redux/todo';
import { clickToUpdate } from '../redux/todo';

const mapActionToProps = (dispatch) => ({
    deleteItem: (id) => {dispatch(deleteItem(id))},
    checkItem: (id) => {dispatch(checkItem(id))},
    tickImportantTask: (id) => {dispatch(tickImportantTask(id))},
    clickToUpdate: (id) => {dispatch(clickToUpdate(id))}
})

export default connect(null, mapActionToProps)(Task,  ToDoList);