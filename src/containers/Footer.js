import { connect } from 'react-redux';

import { showItems } from '../redux/todo';
import { deleteAllItem } from '../redux/todo';

import Footer from '../components/Footer';

const mapStateToProps = (state) => ({
    todos: state.todo.toDoList,
    status: state.todo.status
})

const mapActionToProps = (dispatch) => ({
    deleteAllItem: (list) => {dispatch(deleteAllItem(list))},
    showItems: (status) => {dispatch(showItems(status))},
})

export default connect(mapStateToProps, mapActionToProps)(Footer);