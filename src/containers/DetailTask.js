import { connect } from 'react-redux';

import DetailTask from '../components/DetailTask';

const mapStateToProps = (state) => ({
    todos: state.todo.toDoList
})

export default connect(mapStateToProps)(DetailTask);