import { connect } from 'react-redux';

import { deleteItem } from '../redux/todo';
import { addItem } from '../redux/todo';
import { setItems } from '../redux/todo';
import { fetchItems } from '../redux/todo';

import { viewTodos } from '../selectors/index';

import ToDoList from '../components/ToDoList';

const mapStateToProps = (state) => ({
    todos: viewTodos(state),
    // todos: state.todo.toDoList,
    status: state.todo.status
})

const mapActionToProps = (dispatch) => ({
    addItem: (text) => {dispatch(addItem(text))},
    deleteItem: (id) => {dispatch(deleteItem(id))},
    setItems: (items) => {dispatch(setItems(items))},
    fetchItems: () => {dispatch(fetchItems())},
})

export default connect(mapStateToProps, mapActionToProps)(ToDoList);