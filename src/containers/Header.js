import { connect } from 'react-redux';

import { addItem } from '../redux/todo';
import { clickToUpdated } from '../redux/todo';

import Header from '../components/Header';

const mapStateToProps = (state) => ({
    todos: state.todo.toDoList
})

const mapActionToProps = (dispatch) => ({
    addItem: (text) => {dispatch(addItem(text))},
    clickToUpdated: (text) => {dispatch(clickToUpdated(text))}
})

export default connect(mapStateToProps, mapActionToProps)(Header);