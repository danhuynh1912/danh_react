import { createSelector } from 'reselect';

const getToDoList = (state) => {
    return state.todo.toDoList
};
const setStatus = (state) => state.todo.status;

export const viewTodos = createSelector(
    [setStatus, getToDoList],
    (status, toDoList) => {
        switch(status){
            case "act": 
                return toDoList.filter(item => !item.isCheck);
            case "comp":
                return toDoList.filter(item => item.isCheck);
            default: 
                return toDoList
        }
    }
) 