import { call, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';

const apiUrl = 'https://606134c9ac47190017a70859.mockapi.io/product/toDoList';

// function getApi() {
//     return fetch(apiUrl, {
//         method: 'GET',
//         headers: {
//             'Content-Type': 'application/json',
//         }
//     }).then(res => res.json())
//     .catch(err => {throw err})
// }

function* fetchTodos(action) {
    try {
        const res = yield call(() => axios.get(apiUrl));
        yield put({ type: 'SET_ITEMS', payload: res.data });
    }
    catch(e) {
        yield put({ type: 'GET_ITEMS_FAILED', payload: e.message });
    }
}

function* toDoSaga() {
    yield takeEvery("FETCH_ITEMS", fetchTodos);
}

export default toDoSaga