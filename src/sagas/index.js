import { all } from 'redux-saga/effects'
import toDoSaga from './toDoList';

export default function* rootSaga() {
    yield all([
        toDoSaga(),
    ])
}
