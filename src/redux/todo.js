import axios from 'axios';

var initStateTodo = {
    toDoList: [],
    status: ""
};

const ADD_ITEM = "ADD_ITEM";
const DELETE_ITEM = "DELETE_ITEM";
const SET_ITEMS = "SET_ITEMS";
const DELETE_ALL_ITEM = "DELETE_ALL_ITEM";
const CHECK_ITEM = "CHECK_ITEM";
const SHOW_ITEMS = "SHOW_ITEMS";
const FETCH_ITEMS = "FETCH_ITEMS";
const IMPORTANT_TASK = "IMPORTANT_TASK";
const WILL_UPDATE = "WILL_UPDATE"
const UPDATED = "UPDATED"

export const checkItem = (id) => ({
    type: CHECK_ITEM,
    payload: id
})

export const addItem = (text) => ({
    type: ADD_ITEM,
    payload: text
});

export const deleteItem = (id) => ({
    type: DELETE_ITEM,
    payload: id
})

export const deleteAllItem = (list) => ({
    type: DELETE_ALL_ITEM,
    payload: list
})

export const showItems = (status) => ({
    type: SHOW_ITEMS,
    payload: status
})

export const setItems = (items) => ({
    type: SET_ITEMS,
    payload: items
})

export const fetchItems = () => ({
    type: FETCH_ITEMS
})

export const tickImportantTask = (id) => ({
    type: IMPORTANT_TASK,
    payload: id
})

export const clickToUpdate = (id) => ({
    type: WILL_UPDATE,
    payload: id
})

export const clickToUpdated = (text) => ({
    type: UPDATED,
    payload: text
})

const reducer = (state = initStateTodo, action) => {
    switch(action.type){
        case "ADD_ITEM": {

            return {
                ...state,
                toDoList: [...state.toDoList, {id: state.toDoList.length? String(parseInt(state.toDoList[state.toDoList.length-1].id) + 1):"0", title: action.payload, isCheck: false, doing: false, currentDoing: false, updating: false , important: false, sec: 0, min: 0, timeId: null }]
            }
        }
        case CHECK_ITEM: {
            var index = state.toDoList.map(x => {
                return x.id
            }).indexOf(action.payload)
            return {
                ...state,
                toDoList: [
                    ...state.toDoList.slice(0, index),
                    {...state.toDoList[index], isCheck: !state.toDoList[index].isCheck },
                    ...state.toDoList.slice(index + 1)
                ]
            }
        }
        case DELETE_ITEM: {
            var index = state.toDoList.map(x => {
                return x.id;
            }).indexOf(action.payload);
            // let list = state.toDoList;
            // list.splice(index, 1);
            return {
                ...state,
                toDoList: [
                    ...state.toDoList.slice(0, index),
                    ...state.toDoList.slice(index + 1)
                ]
            }
        }
        case DELETE_ALL_ITEM: {
            return {
                ...state,
                toDoList: action.payload
            }
        }
        case SHOW_ITEMS: {
            return {
                ...state,
                status: action.payload
            } 
        }
        case SET_ITEMS: {
            return {
                ...state,
                toDoList: action.payload
            }
        }
        case FETCH_ITEMS: {
            return {
                ...state
            }
        }
        case IMPORTANT_TASK: {
            var index = state.toDoList.map(item => {
                return item.id
            }).indexOf(action.payload)
            return {
                ...state,
                toDoList: [
                    ...state.toDoList.slice(0, index), 
                    { ...state.toDoList[index], important: !state.toDoList[index].important },
                    ...state.toDoList.slice(index + 1)
                ]
            }
        }
        case WILL_UPDATE: {
            var index = state.toDoList.map(item => {
                return item.id
            }).indexOf(action.payload);
            return {
                ...state,
                toDoList: [
                    ...state.toDoList.slice(0, index),
                    { ...state.toDoList[index], updating: !state.toDoList[index].updating },
                    ...state.toDoList.slice(index + 1)
                ]
            }
        }
        case UPDATED: {
            var itemUpdating = state.toDoList.find(item => item.updating === true);
            var index = state.toDoList.map(item => {
                return item.id
            }).indexOf(itemUpdating.id)
            return {
                ...state,
                toDoList: [
                    ...state.toDoList.slice(0, index),
                    { ...state.toDoList[index], title: action.payload, updating: false },
                    ...state.toDoList.slice(index + 1)
                ]
            }
        }
        default: 
            return state;
    }
};

export default reducer