import { createStore, compose, combineReducers, applyMiddleware } from "redux";
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../sagas/index';

import toDoReducer from './todo';

const sagaMiddleware = createSagaMiddleware();

const reducer = combineReducers({
    todo: toDoReducer
})

const asyncMiddleWare = store => next => action => {
    if(typeof action === "function") {
        return action(next);
    }
    return next(action);
}

const myMiddleware = store => next => action => {
    if(action.type === "ADD_ITEM" && action.payload === "fuck") {
        action.payload = "****"
    }
    return next(action);
}


const store = createStore(
    reducer,
    compose(applyMiddleware(myMiddleware, asyncMiddleWare, sagaMiddleware))
)
    
sagaMiddleware.run(rootSaga);

export default store;