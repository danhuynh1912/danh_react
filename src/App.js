import React, { Component } from "react"
import Nav from './components/Nav';

import { ThemeContext } from './contexts/ThemeContext';
import WithTheme from './components/WithTheme';

const NavTheme = WithTheme(Nav);



class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      theme: localStorage.getItem("theme"),
      isShowStatus: ""
    }
    
    this.changeTheme = this.changeTheme.bind(this);

  }


  componentDidMount() {
    var cssUrl = this.state.theme === "dark"? "/AppDark.css":"/App.css";
    this.addStyle(cssUrl);
  }

  componentDidUpdate() {
    var styleUpdate = document.getElementById('cssok');
    styleUpdate.href = this.state.theme === "dark"? "/AppDark.css" : "/App.css";
  }

  addStyle = url => {
    const style = document.createElement("link");
    style.href = url;
    style.rel = "stylesheet";
    style.async = true;
    style.id = "cssok";

    document.head.appendChild(style);
  };


  changeTheme() {
    const { theme } = this.state;
    console.log("current state: ", theme);
    
    localStorage.setItem("theme", theme === "light"? "dark":"light");
    this.setState({
      theme: localStorage.getItem("theme")
    })
  }

  render() {
    const { theme } = this.state;
    return (
      <ThemeContext.Provider value={{ theme: theme, changeTheme: this.changeTheme }}>
        <div className="myapp">
          <NavTheme />
        </div>
      </ThemeContext.Provider>
    );
  }
}

export default App;
