import React, { Component } from 'react';
import checkIMG from './images/check.svg';
import checkedIMG from './images/checked.svg';
import trash from './images/delete.svg';
import pen from './images/update.svg';
import updateDark from './images/updateDark.svg';
import checkDarkIMG from './images/checkDark.svg';
import checkedDarkIMG from './images/checkedDark.svg';
import trashDark from './images/deleteDark.svg';
import starOn from './images/starOn.svg';
import star from './images/star.svg';
import starDark from './images/starDark.svg';
import play from './images/play.svg';
import pause from './images/pause.svg';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";


class Task extends Component {
    constructor(props){
        super(props);
        this.state = {
            sec: 55,
            min: 0
        }

        // this.startTime = this.startTime.bind(this);
    }

    // static getDerivedStateFromProps(nextProps){
    //     console.log("go getDerivedStateFromProps")
    //     const {item} = nextProps;
    //     const test = toDoList.some((item) => !item.isCheck);
    //     return test? {checkAll: false}:{checkAll: true}
    // }

    render() {
        // const { sec, min } = this.state;
        const { item, tickImportantTask, clickToUpdate, theme, startDoItem, changeCurrentDoing } = this.props;
        const { deleteItem, checkItem } = this.props;
        let tickLight = checkIMG;
        let tickDark = checkDarkIMG;
        let statusTask = "timeStart";
        let statusText = "start";
        let starIMG = item.important? starOn:star;
        if (item.isCheck) {
            tickLight = checkedIMG;
            tickDark = checkedDarkIMG;
            statusTask += " done";
            statusText = "done"
        }
        else if(item.doing) {
            statusTask += " doing";
            statusText = "doing"
        }
        else {
            statusTask += " willStart";
            statusText = "start";
        }
        return (
            <form className={item.isHidden ? "task hidden" : "task"}>

                <input className="inp-cbx" id={item.id} type="checkbox" onChange={e => {}} checked={item.isCheck? "checked":""} onClick={() => checkItem(item.id)} />
                <label className="cbx" for={item.id}><span>
                    <svg width="12px" height="9px" viewbox="0 0 12 9">
                        <polyline points="1 5 4 8 11 1"></polyline>
                    </svg></span><span>{item.title}</span>
                </label>
                <div onClick={startDoItem} className={statusTask}>{statusText}</div>
                <p style={{
                        position: "absolute",
                        opacity: ".5",
                        left: "300px",
                        top: "17px",
                }}>{item.updating? "updating...":null}</p>
                <Link className="detailview" to={{
                    pathname: "/task/" + item.id
                }} >detail</Link>
                <img className={item.important? "important importantOn":"important"} src={theme === "dark"? starDark:starIMG} alt="important" onClick={() => tickImportantTask(item.id)} />
                <img className="play" src={item.currentDoing? pause:play} alt="play" onClick={changeCurrentDoing}/>
                <span className="showTime">{item.min}:{item.sec<10? "0":null}{item.sec}</span>
                <img className="pen"
                    src={theme === "dark" ? updateDark : pen}
                    alt="pen"
                    onClick={() => clickToUpdate(item.id)} />
                <img className="trash"
                    src={theme === "dark" ? trashDark : trash}
                    alt="trash"
                    onClick={() => deleteItem(item.id)} />
            </form>
        )
    }
}

export default Task;