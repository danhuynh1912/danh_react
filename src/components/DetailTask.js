import React, { Component } from 'react';
import { useHistory, useParams } from 'react-router-dom';
// import {
//     BrowserRouter as Router,
//     Switch,
//     Route,
//     Link,
//     useHistory,
//     useLocation,
//     useParams
//   } from "react-router-dom";

export default function DetailTask({todos}) {
    let { id } = useParams();
    let history = useHistory();
    const item = todos.find(task => task.id === id)

    let back = e => {
        e.stopPropagation();
        history.goBack();
    }

    return (
        <div 
            onClick={back}
            style={{
                position: "absolute",
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                background: "rgba(0, 0, 0, 0.15)"
            }}
        >
            <div className="modal" >
                <div className="task-detail">
                     <h1>{item.title}</h1>
                     <p>Status: {item.isCheck? "done":"active"}</p>
                     <p>Is important: yes</p>
                     <p>Time: {item.min}:{item.sec}</p>
                     <button className="close-button" type="button" onClick={back}>
                        Close
                    </button>                
                </div>
            </div>
        </div>
    )

}