import React, { Component } from 'react';
import './static/Theme.css';

export default class Theme extends Component{
    constructor(props){
        super(props);

        this.changeThemeCheckbox = React.createRef();
    }

    render(){
        const { theme, changeTheme } = this.props;
        return(
            <div class="container abouttheme">
                <label class="switch" for="checkbox">
                    <input ref={this.changeThemeCheckbox} 
                        type="checkbox" 
                        id="checkbox" 
                        onClick={changeTheme}/>
                    <div class={theme === "dark"? "slider dark round":"slider round"}></div>
                </label>
            </div>
        )
    }
}