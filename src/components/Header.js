import React, { Component } from "react";
import checkallIMG from './images/checkall.svg';
import checkallDarkIMG from './images/checkAllDark.svg';
import { withCookies } from 'react-cookie';

class Header extends Component {
    constructor(props) {
        super(props);
        const { cookies } = props;
        this.state = {
            newItem: "",
            checkAll: cookies.get("checkAll")
        }
        this.onChange = this.onChange.bind(this);
        this.updateItemOnclick = this.updateItemOnclick.bind(this);
        this.changeCheckAll = this.changeCheckAll.bind(this)

        this.addInput = React.createRef();
        this.reValueInput = this.reValueInput.bind(this);
    }

    static getDerivedStateFromProps(nextProps){
        console.log("go getDerivedStateFromProps")
        const {toDoList} = nextProps;
        const test = toDoList.some((item) => !item.isCheck);
        return test? {checkAll: false}:{checkAll: true}
    }

    componentDidMount(){
        this.addInput.current.focus();
    }

    onChange(event) {
        let text = event.target.value;
        this.setState({
            newItem: text
        })
    }

    addItemOnclick() {
        const { newItem } = this.state;
        const { addItem, checkExist } = this.props;
        if (!checkExist(newItem)){
            addItem(newItem);
            this.setState({
                newItem: ''
            })
        } 
        else {
            alert("this task is already exist");
            this.addInput.current.focus();
        }
        this.setState({
            checkAll: false
        })
    }

    updateItemOnclick() {
        const { newItem } = this.state;
        const { checkExist, updateItem } = this.props;
        if (newItem) {
            if (!checkExist(newItem)) {
                updateItem(newItem);
                
            } else alert("This task is already exist")
        }
        else alert("Task can't be null")

        this.setState({
            newItem: ''
        })
    }

    changeCheckAll(){
        const { checkAllItem, cookies } = this.props
        const { checkAll } = this.state; 
        cookies.set("checkAll", checkAll? false:true, {path: '/'})
        this.setState({
            checkAll: cookies.get("checkAll")
        },() => {
            checkAllItem(checkAll);
        })
        console.log("this is coki", cookies.get("checkAll"))
    }

    reValueInput(text) {
        this.setState({
            newItem: text
        })
    }

    render() {
        const { todos, theme, addItem, clickToUpdated } = this.props;
        const { newItem, checkAll } = this.state;
        let check = todos.length? todos.some((item) => item.updating):null;
        return (
            <div className="addItem">
                <img class={todos.length && checkAll? "tickedAll":""} src={theme === "dark"? checkallDarkIMG:checkallIMG} alt="checkAll"
                    onClick={() => this.changeCheckAll()} />
                <div className="addInput" >
                    <input type="submit" className={check || newItem ? "addButton" : "addButton hidden"} value={ check? "Update":"Add"} onClick={() => {
                        this.setState({
                            newItem: ""
                        })
                        check? clickToUpdated(newItem):addItem(newItem)}} />
                    <input className="addText" type="text"
                        placeholder=""
                        ref={this.addInput}
                        value={newItem}
                        onChange={this.onChange} />

                    <label className="label">
                        <span className={newItem === ""? "span":"span inputHasText"}>What needs to be done</span>
                    </label>
                </div>
            </div>
        )
    }
}

export default withCookies(Header);