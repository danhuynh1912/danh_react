import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Theme from './Theme';
// img
import themeIMG from './images/theme.svg';
import themeDarkIMG from './images/themeDark.svg';
// end img
import WithTheme from './WithTheme';

const ThemeAfter = WithTheme(Theme);

export default class Setting extends Component {
    render() {
        const { theme } = this.props;
        return (
            <div className="col-10 right-content">
                <Router>
                    <div className="title-app">
                        <h1>Setting</h1>
                    </div>
                    <div className="select">

                        {/* <Link to="/setting/yourtask">
                            <div className="option your-task"><img src={theme? yourtaskDarkIMG:yourtaskIMG} />About your task</div>
                        </Link> */}

                        <Link to="/setting/theme">
                            <div className="option theme"><img alt="theme" src={theme === "dark"? themeDarkIMG:themeIMG} />Theme</div>
                        </Link>
                    </div>

                    <Switch>
                        <Route path="/setting/theme">
                            <ThemeAfter />
                        </Route>
                    </Switch>
                </Router>
            </div>
        )
    }
}