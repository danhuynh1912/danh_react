import React, { Component } from 'react';


class Footer extends Component {

    // shouldComponentUpdate(nextProps, nextState){
    //     if(nextState.isShowStatus === this.state.isShowStatus && nextProps.itemleft === this.props.itemleft) return false
    //     else return true;
    // }

    render() {
        console.log("Footer updated")
        const { deleteAllItem, todos, showItems, status } = this.props;
        var actionItems = todos.filter(item => !item.isCheck);
        var completedItems = todos.filter(item => item.isCheck);
        return (
            <div className="footer">
                <div className='button-option'>
                    <button className={status === "" ? "status-active" : ""} onClick={() => showItems("")}>All</button>
                    <button className={status === "act" ? "status-active" : ""} onClick={() => showItems("act")}>Action</button>
                    <button className={status === "comp" ? "status-active" : ""} onClick={() => showItems("comp")}>Completed</button>
                </div>
                <p className="item-left"> {actionItems.length} items left</p>
                {completedItems.length > 0 && <button className={!completedItems.length ? "deleteAll hidden" : "deleteAll"} onClick={() => deleteAllItem(actionItems)}>Clear compleled</button>}
            </div>
        )
    }
}

export default Footer;