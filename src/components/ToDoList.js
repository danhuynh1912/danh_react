import React, { Component } from 'react';
// import { FixedSizeList as List } from 'react-window';
import { CookiesProvider } from 'react-cookie';
import axios from 'axios';
// import Task from './Task';
import Task from '../containers/Task';
// import Footer from './Footer';
import Footer from '../containers/Footer';
// import Header from './Header';
import Header from '../containers/Header';

import WithTheme from './WithTheme';

const TaskAfter = WithTheme(Task);
const HeaderAfter = WithTheme(Header);


class ToDoList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            toDoList: []
        }

        this.footerRef = React.createRef();
        this.headerRef = React.createRef();
        this.chatListRef = React.createRef();
        this.taskRef = React.createRef();
    };

    componentDidMount() {

        this.props.fetchItems();
    }

    reRenderParentCallBack() {
        this.forceUpdate();
    }

    playTime(toDoList, index, item){
        item.timeId = setInterval(() => {
            item.sec = item.sec > 58? 0:item.sec + 1
            item.min = item.sec > 58? item.min + 1:item.min

            this.setState({
                toDoList: toDoList
            })

            axios.put(`http://localhost:8000/toDoItems/${item.id}`, item)
                .then(res => 
                    console.log(res))
                .catch(res => 
                    console.log(res))
        }, 1000);     
        
    }

    changeCurrentDoing(item, index){
        const { toDoList } = this.state;
        item.isCheck = item.isCheck && !item.currentDoing? !item.isCheck:item.isCheck;
        item.currentDoing = !item.currentDoing;
        item.doing = true;
        this.setState({
            toDoList: toDoList
        })
        
        axios.put(`http://localhost:8000/toDoItems/${item.id}`, item)
            .then(res => 
                console.log(res))
            .catch(res => 
                    console.log(res))
        item.currentDoing? this.playTime(toDoList, index, item):clearInterval(item.timeId);
    }

    render() {
        // let location = useLocation();

        const { toDoList } = this.state;
        const { addItem, deleteItem, todos } = this.props;
        
        let itemleft = toDoList.filter((item) => !item.isCheck);
        console.log("item left: ", itemleft);

        let itemChecked = toDoList.some((item) => item.isCheck) //true false

        return (
            <CookiesProvider>

            <div className="col-10 right-content">
                <div className="title-app">
                    <h1>To do app</h1>
                </div>
                <div className="todolist">
                    <HeaderAfter
                        checkAllItem={this.checkAllItem}
                        updateItem={this.updateItem}
                        checkExist={this.checkExist}
                        // addItem={this.addItem}
                        theRef={this.headerRef}
                        toDoList={toDoList}
                        changeCheckAll={this.changeCheckAll}
                        reRenderParentCallBack={this.reRenderParentCallBack}
                        tickAllClass={toDoList.length && !itemleft.length ? "tickedAll" : ""}
                    />
                    <div className="list">

                        {/* {todos.length > 0 &&
                            todos.map((item, index) => {
                                let flag = false;
                                const buttonCheck = this.props.status;
                                if (
                                    buttonCheck === "" ||
                                    (buttonCheck === "act" && !item.isCheck) ||
                                    (buttonCheck === "comp" && item.isCheck)
                                ) {
                                    flag = true;
                                }
                                return flag ? <TaskAfter key={this.index} item={item}
                                    theRef={this.taskRef}
                                    checkOnClick={() => this.checkItem(item, index, item.id)}
                                    renameOnClick={() => this.renameOnClick(item, index, item.id)}
                                    importantTask={() => this.importantTask(item, index)}
                                    changeCurrentDoing={() => this.changeCurrentDoing(item, index, item)}
                                    removeOnClick={() => this.removeItem(item, index, item.id)} /> : null;
                            })
                        } */}
                        { todos.length > 0 &&
                            todos.map((item, index) => <TaskAfter key={this.index} item={item}
                                theRef={this.taskRef}
                                changeCurrentDoing={() => this.changeCurrentDoing(item, index, item)} />)    
                        }
                    </div>

                    <div className="hintScroll"></div>

                    {!todos.length && <h1 className="empty">Not thing here</h1>}
                    <Footer
                        reRenderParentCallBack={this.reRenderParentCallBack}
                        itemleft={itemleft}
                        deleteOnClick={this.deleteOnClick}
                        deleteAll={!toDoList.length ? "deleteAll hidden" : "deleteAll"}
                        atLeastChecked={itemChecked}
                        ref={this.footerRef}
                    />
                </div>
            </div>
            </CookiesProvider>

        )
    }
}

export default ToDoList;