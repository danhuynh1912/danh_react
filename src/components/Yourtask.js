import axios from 'axios';
import React, { Component } from 'react';
import starOn from './images/starOn.svg';
import { Row, Col } from 'reactstrap';

export default class Yourtask extends Component{
    constructor(props){
        super(props);
        this.state = {
            yourTask: []
        }
    }

    componentDidMount(){
        axios.get("http://localhost:8000/toDoItems").then(res => {
            this.setState({
                yourTask: res.data
            })
        })
    }

    statusItems(item){
        let mark = "colorMark";
        if(item.isCheck) mark += " markDone";
        else{
            if(item.doing) mark += " markDoing";
            else mark += " markStart";
        }
        return mark
    }

    render(){
        const { yourTask } = this.state;
        return(
            <div className="col-10 right-content alltasks">
                <div className="title-app">
                    <h1>Your tasks</h1>
                </div>
                <Row className="taskslist">
                    { yourTask.length > 0 && yourTask.map((item, index) => 
                        <Col xs="3">
                            <div className="main card">
                                <h4>{item.title}</h4>
                                <p>Period: <span>{item.min}:{item.sec<10? "0":null}{item.sec}</span></p>
                                <p>Status: <span>{item.isCheck? "Finished":"Doing"}</span></p>
                                <img className="importantList" src={item.important? starOn:null} />
                                <span className={this.statusItems(item)}></span>
                            </div>
                        </Col>
                    ) }
                </Row>
            </div>
        )
    }
}