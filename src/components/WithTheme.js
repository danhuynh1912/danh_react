import React, { Component } from 'react';

import { ThemeContext } from '../contexts/ThemeContext';

export default function acb(ComponentWithThisTheme) {
    return class extends Component {
        
        render() {
            return (
                <ThemeContext.Consumer>
                    {({theme, changeTheme}) => <ComponentWithThisTheme {...this.props} theme={theme} changeTheme={changeTheme} ref={this.props.theRef} />}
                </ThemeContext.Consumer>
            );
        }
    }
} 