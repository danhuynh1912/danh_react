import React, { Component } from "react";
import {
    Navbar,
    NavbarBrand,
    Nav,
    NavItem,
} from 'reactstrap';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams,
    useLocation
} from "react-router-dom";
// import ToDoList from './ToDoList';
import ToDoList from '../containers/ToDoList';
import Setting from './Setting';
import Yourtasks from './Yourtask';
import WithTheme from './WithTheme';
import DetailTask from '../containers/DetailTask';

import homeIMG from './images/home.svg';
import homeIMGDark from './images/homeDark.svg';
import settingsIMG from './images/settings.svg';
import settingsIMGDark from './images/settingsDark.svg';
import yourtasksIMG from './images/yourtask.svg';
import yourtasksIMGDark from './images/yourtaskDark.svg';

const SettingAfter = WithTheme(Setting);

class NavbarCom extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: "home"
        }
    }

    activePage(value){
        this.setState({
            currentPage: value
        })
    }

    render() {
        const { currentPage } = this.state;
        return (
            <div className="row contentapp">
                <Router>
                    <Navbar className="col-2" color="" light expand="md">
                        <NavbarBrand href="/">Todoapp</NavbarBrand>
                        <Nav className="mr-auto" navbar>
                            <NavItem className={currentPage === "home"? "nav-item nav-active":"nav-item"}>
                                <Link to="/" onClick={() => this.activePage("home")}><img alt="iconNav" src={this.props.theme === "dark"? homeIMGDark : homeIMG} />Home</Link>
                            </NavItem>
                            <NavItem className={currentPage === "set"? "nav-item nav-active":"nav-item"}>
                                <Link to="/setting" onClick={() => this.activePage("set")}><img alt="iconNav" src={this.props.theme === "dark"? settingsIMGDark : settingsIMG} />Setting</Link>
                            </NavItem>
                            <NavItem className={currentPage === "task"? "nav-item nav-active":"nav-item"}>
                                <Link to="/yourtasks" onClick={() => this.activePage("task")}><img alt="iconNav" src={this.props.theme === "dark"? yourtasksIMGDark : yourtasksIMG} />About your tasks</Link>
                            </NavItem>
                        </Nav>
                    </Navbar>

                    <Switch>
                        <Route path="/setting">
                            <SettingAfter />
                        </Route>
                        <Route path="/yourtasks">
                            <Yourtasks />
                        </Route>
                        <Route path="/task/:id">
                            <DetailTask />
                        </Route>
                        <Route path="/">
                            <ToDoList />
                        </Route>
                    </Switch>
                </Router>
            </div>
        );
    }

}

export default NavbarCom;